﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Interfaces
{
    /// <summary>
    /// Interface de Base para ser implementada con los métodos de Servicios en el consumo de una API externa
    /// </summary>
    /// <typeparam name="T">Clase Parametro</typeparam>
    public interface IBaseServiceModel<T>
    {
        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de Lista de Objetos por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Lista de Objetos Serializados</returns>
        Task<IList<T>> GetList(string endPoint, params string[] requestsParameters);

        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de Lista de Objetos por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Lista de Objetos Serializados</returns>
        Task<IList<T>> GetList(string endPoint, Dictionary<string, string> requestsParameters = null);

        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto Serializado</returns>
        Task<T> Get(string endPoint, params string[] requestsParameters);

        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto Serializado</returns>
        Task<object> GetAsObject(string endPoint, Dictionary<string, string> requestsParameters = null);

        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto Serializado</returns>
        Task<T> Get(string endPoint, Dictionary<string, string> requestsParameters = null);

        /// <summary>
        /// Método asíncrono de implementación Base para Crear un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="bodyObject">Objeto que se inserta en el body</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto de Respuesta Serializado</returns>
        Task<object> Create(string endPoint, T bodyObject, params string[] requestsParameters);

        /// <summary>
        /// Método asíncrono de implementación Base para Crear un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="bodyObject">Objeto que se inserta en el body</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto de Respuesta Serializado</returns>
        Task<object> Create<V>(string endPoint, V bodyObject, params string[] requestsParameters);

        /// <summary>
        /// Método asíncrono de implementación Base para Actualizar un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="bodyObject">Objeto que se inserta en el body</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto de Respuesta Serializado</returns>
        Task<object> Update(string endPoint, T bodyObject, params string[] requestsParameters);

        /// <summary>
        /// Método asíncrono de implementación Base para la obtención de un Objeto por medio del servicio
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestsParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Objeto de Respuesta Serializado</returns>
        Task<object> Delete(string endPoint, params string[] requestsParameters);
    }
}
