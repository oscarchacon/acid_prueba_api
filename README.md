# Aplicacion API en .Net 5

## Requisitos para la ejecución
 - Net 5 SDK
 - Visual Studio 2019 / JetBrains Rider 2021
 - Docker desktop: para la ejecución de su contenedor en docker
 - Postman: para ver las ejecuciones de cada endpoint.

## Liberías Necesarias (Nuget)
 - Microsoft.AspNetCore.Authentication.JwtBearer 5.0.5
 - Microsoft.VisualStudio.Azure.Containers.Tools.Targets 1.10
 - Okta.Sdk 5.0.0
 - Swashbuckle.AspNetCore 6.1.2
 - Swashbuckle.AspNetCore.Annotations 6.1.2    
 - Swashbuckle.AspNetCore.Swagger 6.1.2
 - Swashbuckle.AspNetCore.SwaggerGen 6.1.2
 - Swashbuckle.AspNetCore.SwaggerUI 6.1.2
 - Swashbuckle.Core 5.6.0


## Variables de Entorno

 - Audience: URL para la verificación del Token
 - Authority: URL para la autentificación del token utilizando OAuth2
 - ClientId: Id de Cliente que utilizará el SDK de OKTA
 - ClientSecret: Clave secreta para el uso del SDK de OKTA
 - TokenUrl: URL para la obtención del Token
 - Token: Token proporcionado para la utilización del SDK de OKTA
 - Domain: Dominio o URL de la organización creada en la consola de administación de OKTA

 Para saber más de la configuración en OKTA, es necesario leer las siguientes instrucciones descritas aqui: [Configuración Okta](OktaConfiguration.md)


## Ejecución de la API

Para la Ejecución de la API y la documentación de esta, se creó un Swagger explicando el detalle de cada enpoint que se puede ejecutar.

![](Media/Execute/image1.png)

### Enpoint: GET /api/Users/ - Listar Usuarios

Como se ve en la imagen, al usar el endpoint no es necesario, ingresar un header o token, ya que internamente utiliza uno. Para la ejecución de listar usuarios es solo ingresar con URI y ejecutar el postman.

![](Media/Execute/image2.png)

### Enpoint: GET /api/Users/{idUsuario} - Obtener Usuario por ID

Para la ejecución de la obtención de un usuario es solo ingresar con URI y su Id de usuario en el path.

![](Media/Execute/image3.png)

### Enpoint: POST /api/Users/Create -- Crear un usuario

Para la ejecución de la creación de un usuario es solo ingresar con URI y su body ingresar el objeto como JSON de la siguiente manera:
``` JSON
{
    "firstName": "string",
    "lastName": "string",
    "email": "string",
    "password": "string"
}
```

![](Media/Execute/image4.png)

Una vez ejecutado, debe aparecer la respuesta de la API de Okta que se pudo crear al usuario y enviar el objeto de este en su cuerpo. Además para corroborar, puede visitar la consola de administración en la pagina de la organización en OKTA.

![](Media/Execute/image5.png)

Y para corroborar que se ingreso el usuario con la contraseña indicada, es cosa de ingresar en el proyecto de frontend, ejecutarlo y colocar las credenciales necesarias para ver si realmente funcionó.

![](Media/Execute/image6.png)

![](Media/Execute/image7.png)

### Enpoint: PUT /api/Users/{idUsuario}/Edit -- Editar un usuario

Para la ejecución de la edición de un usuario es solo ingresar con URI, el id de usuario en PATH y su body ingresar el objeto como JSON de la siguiente manera:
``` JSON
{
    "title": "string",
    "timezone": "string",
    "streetAddress": "string",
    "state": "string",
    "secondEmail": "string",
    "profileUrl": "string",
    "primaryPhone": "string",
    "preferredLanguage": "string",
    "postalAddress": "string",
    "organization": "string",
    "nickName": "string",
    "mobilePhone": "string",
    "middleName": "string",
    "managerId": "string",
    "manager": "string",
    "locale": "string",
    "lastName": "string",
    "honorificSuffix": "string",
    "honorificPrefix": "string",
    "firstName": "string",
    "employeeNumber": "string",
    "division": "string",
    "displayName": "string",
    "department": "string",
    "countryCode": "string",
    "costCenter": "string",
    "city": "string",
    "userType": "string",
    "zipCode": "string"

}
```

![](Media/Execute/image8.png)

Despues de la ejecución de este endpoint, puede corroborar los cambios en la consola de administración de la organización en OKTA, tal como se muestra en la siguientes imagenes.

![](Media/Execute/image9.png)

![](Media/Execute/image10.png)

### Enpoint: PUT /api/Users/{idUsuario}/ChangePassword -- Cambiar la contraseña de un usuario

Para la ejecución de la edición de un usuario es solo ingresar con URI, el id de usuario en PATH y su body ingresar el objeto como JSON de la siguiente manera:
``` JSON
{
    "newPassword": "string",
    "confirmNewPassword": "string",
    "oldPassword": "string"
}
```

![](Media/Execute/image11.png)

### Enpoint: DELETE /api/Users/{idUsuario}/Delete -- Eliminar a un usuario

Para la ejecución de la edición de un usuario es solo ingresar con URI, el id de usuario en PATH.

![](Media/Execute/image12.png)

Una vez ejecutado el Endpoint, puede corroborar que el usuario fue eliminado en la consola de administración de la organización en OKTA, tal como muestra la siguiente imagen:

![](Media/Execute/image13.png)

![](Media/Execute/image14.png)
