﻿using Newtonsoft.Json;
using Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    /// <summary>
    /// Clase que sirve de base para los servicios a consumir
    /// </summary>
    public abstract class BaseService
    {
        readonly HttpClient _httpClient;
        private readonly ExternService _externService;

        /// <summary>
        /// Constructor de la clase que crea las cabeceras necesarias para las peticiones
        /// </summary>
        /// <param name="externService">Clase de Consumo de Api Externa, previamente configurada</param>
        public BaseService(ExternService externService)
        {
            _externService = externService;
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(externService.MediaType));
            if (externService.Key != null && !externService.Key.Equals(string.Empty))
            {
                _httpClient.DefaultRequestHeaders.Add("Key", externService.Key);
            }
            if (externService.Token != null && !externService.Token.Equals(string.Empty))
            {
                _httpClient.DefaultRequestHeaders.Add("Bearer", externService.Token);
            }
            if (externService.Headers != null && externService.Headers.Any())
            {
                foreach (var header in externService.Headers)
                {
                    _httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
        }

        /// <summary>
        /// Método asíncrono Base para las peticiones de tipo Post
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="bodyObject">Objeto parametrizado en el Body</param>
        /// <param name="requestParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Respuesta de la petición</returns>
        public async Task<HttpResponseMessage> PostAction(string endPoint, object bodyObject, ParameterCollection requestParameters = null)
        {
            var uri = this.UriCreate(endPoint, requestParameters);
            var json = JsonConvert.SerializeObject(bodyObject);

            var bodyContent = new StringContent(json, Encoding.UTF8, _externService.MediaType);

            HttpResponseMessage response = null;

            response = await _httpClient.PostAsync(uri, bodyContent);

            return response;
        }

        /// <summary>
        /// Método asíncrono Base para las peticiones de tipo Put
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="bodyObject">Objeto parametrizado en el Body</param>
        /// <param name="requestParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Respuesta de la petición</returns>
        public async Task<HttpResponseMessage> PutAction(string endPoint, object bodyObject, ParameterCollection requestParameters = null)
        {
            var uri = this.UriCreate(endPoint, requestParameters);
            var json = JsonConvert.SerializeObject(bodyObject);
            var bodyContent = new StringContent(json, Encoding.UTF8, _externService.MediaType);

            HttpResponseMessage response = null;

            response = await _httpClient.PutAsync(uri, bodyContent);

            return response;
        }

        /// <summary>
        /// Método asíncrono Base para las peticiones de tipo Get
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Respuesta de la petición</returns>
        public async Task<HttpResponseMessage> GetAction(string endPoint, ParameterCollection requestParameters = null)
        {
            var uri = this.UriCreate(endPoint, requestParameters);
            var response = await _httpClient.GetAsync(uri);

            return response;
        }

        /// <summary>
        /// Método asíncrono Base para las peticiones de tipo Delete
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Respuesta de la petición</returns>
        public async Task<HttpResponseMessage> DeleteAction(string endPoint, ParameterCollection requestParameters = null)
        {
            var uri = this.UriCreate(endPoint, requestParameters);
            var response = await _httpClient.DeleteAsync(uri);

            return response;
        }

        /// <summary>
        /// Método que permite crear la Uri con el endpoint y los parametros de solicitud
        /// </summary>
        /// <param name="endPoint">EndPoint que se inserta a la URL Base</param>
        /// <param name="requestParameters">Parametros de consulta junto con el Endpoint</param>
        /// <returns>Uri Creada para la utilización de las peticiones</returns>
        private Uri UriCreate(string endPoint, ParameterCollection requestParameters = null)
        {
            StringBuilder uriConstructor = new StringBuilder($"{_externService.Url}/{endPoint}");
            if (requestParameters != null)
            {
                if (!endPoint.Contains("?")) { uriConstructor.Append("?"); }
                else { uriConstructor.Append("&"); }
                uriConstructor.Append(requestParameters.ToString());
            }
            var uri = new Uri(string.Format(uriConstructor.ToString(), string.Empty));
            return uri;
        }
    }
}
