﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Utils
{
    public class ExternService
    {
        public string Url { get; set; }

        public string MediaType { get; set; }

        public string Key { get; set; }

        public string Token { get; set; }

        public IList<HeadersModel> Headers { get; set; }
    }

    public class HeadersModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
