﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Utils.Authorization.Models
{
    public class OktaModel
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
    }
}
