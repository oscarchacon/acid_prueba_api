﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Utils
{
    /// <summary>
    /// Clase de Utilidad para el uso de los parametros de solicitudes que se usarán con una URL
    /// </summary>
    public class ParameterCollection
    {
        private readonly Dictionary<string, string> parameters = new Dictionary<string, string>();

        /// <summary>
        /// Método que permite agregar una clave y un valor para usarse conjunto con las solicitudes
        /// </summary>
        /// <param name="key">Clave para usarse en la solicitud</param>
        /// <param name="value">Valor de la clave para ajuntarse en la solicitud</param>
        public void Add(string key, string value)
        {
            if (this.parameters.ContainsKey(key))
            {
                throw new InvalidOperationException(string.Format("The Key {0} exists.", key));
            }
            this.parameters.Add(key, value);
        }

        /// <summary>
        /// Método que permite agregar una clave y un valor para usarse conjunto con las solicitudes
        /// </summary>
        /// <param name="parameters">Lista de pares "Clave-Valor" para ajuntarse en la solicitud</param>
        public void Add(Dictionary<string, string> parameters)
        {
            foreach (var element in parameters)
            {
                this.parameters.Add(element.Key, element.Value);
            }
        }

        /// <summary>
        /// Método de sobrecarga para crear una cadena con los parametros puestos en la solicitud
        /// </summary>
        /// <returns>Cadena de los parametros</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var element in this.parameters)
            {
                if (sb.Length > 0) { sb.Append("&"); }
                sb.Append($"{element.Key}={element.Value}");
            }
            return sb.ToString();
        }
    }
}
