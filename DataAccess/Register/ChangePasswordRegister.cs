﻿using DataAccess.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DataAccess.Register
{
    public class ChangePasswordRegister
    {
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
        public string OldPassword { get; set; }

        [SwaggerIgnore, JsonIgnore]
        public bool PasswordMatch
        {
            get => NewPassword == ConfirmNewPassword;
        }
    }
}
