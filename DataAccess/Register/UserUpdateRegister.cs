﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Register
{
    public class UserUpdateRegister
    {
        public string Title { get; set; }
        public string Timezone { get; set; }
        public string StreetAddress { get; set; }
        public string State { get; set; }
        public string SecondEmail { get; set; }
        public string ProfileUrl { get; set; }
        public string PrimaryPhone { get; set; }
        public string PreferredLanguage { get; set; }
        public string PostalAddress { get; set; }
        public string Organization { get; set; }
        public string NickName { get; set; }
        public string MobilePhone { get; set; }
        public string MiddleName { get; set; }
        public string ManagerId { get; set; }
        public string Manager { get; set; }
        public string Locale { get; set; }
        public string LastName { get; set; }
        public string HonorificSuffix { get; set; }
        public string HonorificPrefix { get; set; }
        public string FirstName { get; set; }
        public string EmployeeNumber { get; set; }
        public string Division { get; set; }
        public string DisplayName { get; set; }
        public string Department { get; set; }
        public string CountryCode { get; set; }
        public string CostCenter { get; set; }
        public string City { get; set; }
        public string UserType { get; set; }
        public string ZipCode { get; set; }
    }
}
