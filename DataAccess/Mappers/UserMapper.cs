﻿using DataAccess.Register;
using Okta.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Mappers
{
    public static class UserMapper
    {
        public static void Map(this IUserProfile userProfile, UserUpdateRegister userUpdate)
        {
            userProfile.Title = userUpdate.Title;
            userProfile.Timezone = userUpdate.Timezone;
            userProfile.StreetAddress = userUpdate.StreetAddress;
            userProfile.State = userUpdate.State;
            userProfile.SecondEmail = userUpdate.SecondEmail;
            userProfile.ProfileUrl = userUpdate.ProfileUrl;
            userProfile.PrimaryPhone = userUpdate.PrimaryPhone;
            userProfile.PreferredLanguage = userUpdate.PreferredLanguage;
            userProfile.PostalAddress = userUpdate.PostalAddress;
            userProfile.Organization = userUpdate.Organization;
            userProfile.NickName = userUpdate.NickName;
            userProfile.MobilePhone = userUpdate.MobilePhone;
            userProfile.MiddleName = userUpdate.MiddleName;
            userProfile.ManagerId = userUpdate.ManagerId;
            userProfile.Manager = userUpdate.Manager;
            userProfile.Locale = userUpdate.Locale;
            userProfile.LastName = userUpdate.LastName;
            userProfile.HonorificSuffix = userUpdate.HonorificSuffix;
            userProfile.HonorificPrefix = userUpdate.HonorificPrefix;
            userProfile.FirstName = userUpdate.FirstName;
            userProfile.EmployeeNumber = userUpdate.EmployeeNumber;
            userProfile.Division = userUpdate.Division;
            userProfile.DisplayName = userUpdate.DisplayName;
            userProfile.Department = userUpdate.Department;
            userProfile.CountryCode = userUpdate.CountryCode;
            userProfile.CostCenter = userUpdate.CostCenter;
            userProfile.City = userUpdate.City;
            userProfile.UserType = userUpdate.UserType;
            userProfile.ZipCode = userUpdate.ZipCode;
        }
    }
}
