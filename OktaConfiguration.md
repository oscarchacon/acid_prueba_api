# Pasos para crear una cuenta en OKTA y obtener las credenciales.

Para crear una cuenta y organización en Okta es necesario ir a la página: <https://developer.okta.com/signup/>

![](Media/Okta/image1.png)

Una vez ingresado los datos, la organización y la cuenta se habrán creado y pedirá el inico de sesión en la plataforma de Okta

![](Media/Okta/image2.png)

Una vez logueado, en la pantalla de inicio saldrá un dashboard para la gestión y la monitorización de las cuentas y aplicaciones asociadas.

![](Media/Okta/image3.png)

Para poder crear una aplicación o asociar una aplicación, es necesario ir a la pestaña "Applications" y luego hacer clic en el botón "Add Application".

![](Media/Okta/image4.png)
![](Media/Okta/image5.png)

En mi caso, cuando agregué la cuenta, lo hice para una Single Page Application (SPA), siguiendo el wizard. Esto crea la aplicación, la cual mostrará los diferentes parámetros que tendrán que añadirse a la aplicación que se esta programando.

![](Media/Okta/image6.png)

Dado que, para que las funcionalidades de la API de Okta y el SDK, funcionen con nuestras aplicaciones es necesario ir a la pestaña "Security" y luego a "API", como se muestra a continuación.

![](Media/Okta/image7.png)

Para que se integre la funcionalidad de la API y SDK de Okta, es necesario crear un Token para poder añadirlo a la aplicación que se necesite. Para eso es se hace clic en la pestaña Tokens, donde aparecerán los diferentes tokens que se agreguen.

![](Media/Okta/image8.png)

Para crear un Token, es necesario hacer clic en el botón "Create Token", la cual aparecerá la siguiente imagen, donde se debe colocar un nombre.

![](Media/Okta/image9.png)

Una vez puesto el nombre, se hace clic en el botón "Create Token", la cual generará un Token, que debe ser copiado para agregarlo a las configuraciones de las aplicaciones que se estén desarrollando.

![](Media/Okta/image10.png)