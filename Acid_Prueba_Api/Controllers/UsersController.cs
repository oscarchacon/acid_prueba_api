﻿using DataAccess.Mappers;
using DataAccess.Register;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Okta.Sdk;
using Okta.Sdk.Configuration;
using Services.Utils.Authorization.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acid_Prueba_Api.Controllers
{
    /// <summary>
    /// Clase Controlador para métodos de Usuario
    /// </summary>
    [SwaggerTag("Controlador para métodos de Usuario")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly OktaConfigModel _oktaSettings;
        private readonly OktaClientConfiguration _oktaClientConfig;

        public UsersController(OktaConfigModel oktaSettings)
        {
            _oktaSettings = oktaSettings;
            _oktaClientConfig = new OktaClientConfiguration
            {
                OktaDomain = oktaSettings.Domain,
                Token = oktaSettings.Token,
                ClientId = oktaSettings.ClientId
            };
        }

        /// <summary>
        /// API que permite obtener todos los datos de los Usuarios
        /// </summary>
        /// <returns>Lista de Usuarios</returns>
        /// <response code="200">Lista de Usuarios</response>   
        /// <response code="204">Usuarios no encontrados</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="500">Error Interno del servidor</response> 
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<IUser>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);

                var users = await oktaClient.Users.ToListAsync();

                if (users == null || !users.Any()) { return NoContent(); }

                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que retorna un Usuario a partir de su Id
        /// </summary>
        /// <param name="id">Id de Usuario</param>
        /// <returns>Objeto Usuario</returns>
        /// <response code="200">Objeto Usuario</response>   
        /// <response code="204">Usuario no encontrado</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="500">Error Interno del servidor</response>   
        [HttpGet("{id}", Name = "UserById")]
        [ProducesResponseType(typeof(IUser), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);
                var user = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Id.Equals(id));

                if (user == null) { return NoContent(); }

                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que crea un nuevo Usuario
        /// </summary>
        /// <param name="userRegister">Objeto Usuario para registro</param>
        /// <returns>Objeto Usuario Creado</returns>
        /// <response code="201">Objeto Usuario Creado</response>
        /// <response code="400">Petición Erronea, Objeto no válido</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="500">Error Interno del servidor</response>   
        [HttpPost("Create")]
        [ProducesResponseType(typeof(IUser), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody] UserRegister userRegister)
        {
            if (userRegister == null) { return BadRequest("User Object is Null"); }
            if (!ModelState.IsValid) { return BadRequest("Invalid Object Model"); }
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);

                var findUser = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Profile.Email.Equals(userRegister.Email));

                if (findUser != null)
                    return BadRequest("Email is register.");

                var profile = new UserProfile
                {
                    FirstName = userRegister.FirstName,
                    LastName = userRegister.LastName,
                    Email = userRegister.Email,
                    Login = userRegister.Email
                };

                var userWithPassword = new CreateUserWithPasswordOptions
                {
                    Profile = profile,
                    Password = userRegister.Password,
                    Activate = true
                };

                var user = await oktaClient.Users.CreateUserAsync(userWithPassword);
                if (user == null || String.IsNullOrEmpty(user.Id)) 
                    return BadRequest(new { Message = "Entity Object is not Created" });

                return CreatedAtRoute("UserById", new { id = user.Id }, user);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Server Error: {ex.Message}.");
            }
        }

        /// <summary>
        /// Api que actualiza los datos de un Usuario existente registrado en el sistema
        /// </summary>
        /// <param name="id">Id Usuario</param>
        /// <param name="userUpdate">Objeto Usuario, con los datos actualizados</param>
        /// <returns>Objeto Usuario con datos actualizados</returns>
        /// <response code="200">Objeto Usuario con Datos Actualizados</response>
        /// <response code="400">Petición Erronea, Objeto no válido, Id no válido</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="404">Datos no encontrados</response>   
        /// <response code="500">Error Interno del servidor</response>   
        [HttpPut("{id}/Edit")]
        [ProducesResponseType(typeof(IUser), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Put(string id, [FromBody] UserUpdateRegister userUpdate)
        {
            if (String.IsNullOrEmpty(id)) { return BadRequest(new { Message = "Id is Empty" }); }
            if (userUpdate == null) { return BadRequest(new { Message = "Entity Object is Null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);

                var findUserId = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Id.Equals(id));
                if (findUserId == null)
                    return NotFound("User not Found");

                if (!String.IsNullOrEmpty(userUpdate.SecondEmail))
                {
                    var findUserEmail = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Profile.Email.Equals(userUpdate.SecondEmail) || usr.Profile.SecondEmail.Equals(userUpdate.SecondEmail));
                    if (findUserEmail != null)
                        return BadRequest(new { Message = "Email is Register" });
                }

                findUserId.Profile.Map(userUpdate);

                var result = await findUserId.UpdateAsync();

                if (String.IsNullOrEmpty(result.Id))
                    return BadRequest(new { Message = "User was not updated" });

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que actualiza la contraseña de un usuario registrado
        /// </summary>
        /// <param name="id">Id Usuario</param>
        /// <param name="userUpdate">Objeto Usuario, con los datos actualizados</param>
        /// <returns>Objeto Usuario con datos actualizados</returns>
        /// <response code="200">Objeto Usuario con Datos Actualizados</response>
        /// <response code="400">Petición Erronea, Objeto no válido, Id no válido</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="404">Datos no encontrados</response>   
        /// <response code="500">Error Interno del servidor</response>   
        [HttpPut("{id}/ChangePassword")]
        [ProducesResponseType(typeof(IUser), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutChangePassword(string id, [FromBody] ChangePasswordRegister userPasswords)
        {
            if (String.IsNullOrEmpty(id)) { return BadRequest(new { Message = "Id is Empty" }); }
            if (userPasswords == null) { return BadRequest(new { Message = "Entity Object is Null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            if (!userPasswords.PasswordMatch) { return BadRequest(new { Message = "New Password not Match" }); }
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);

                var findUserId = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Id.Equals(id));

                if (findUserId == null)
                    return NotFound("User not Found");

                var result = await findUserId.ChangePasswordAsync(new ChangePasswordOptions 
                {
                    NewPassword = userPasswords.NewPassword,
                    CurrentPassword = userPasswords.OldPassword
                });

                return Ok(new { Message = "User password is updated" });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que permite eliminar registro de Usuario a partir de su Id
        /// </summary>
        /// <param name="id">Id Usuario</param>
        /// <returns>Sin contenido</returns>
        /// <response code="204">Objeto Usuario eliminado, sin contenido</response>
        /// <response code="400">Petición Erronea, Id no válido</response>   
        /// <response code="401">Sin Autorización</response>   
        /// <response code="403">Sin Privilegios</response>   
        /// <response code="404">Datos no encontrados</response>   
        /// <response code="405">No se permite borrar registro</response>   
        /// <response code="500">Error Interno del servidor</response>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(object), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(object), StatusCodes.Status405MethodNotAllowed)]
        [ProducesResponseType(typeof(object), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) { return BadRequest(new { Message = "Id is Empty" }); }
            try
            {
                var oktaClient = new OktaClient(_oktaClientConfig);

                var findUserId = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Id.Equals(id));

                if (findUserId == null)
                    return NotFound("User not Found");

                await findUserId.DeactivateAsync();
                await findUserId.DeactivateOrDeleteAsync();

                var findUserIdDelete = await oktaClient.Users.FirstOrDefaultAsync(usr => usr.Id.Equals(id));

                if (findUserIdDelete != null)
                    return StatusCode(StatusCodes.Status405MethodNotAllowed, new { Message = "Not allowed to delete Entity registry." });

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = $"Internal server error: {ex.Message}" });
            }
        }
    }
}
