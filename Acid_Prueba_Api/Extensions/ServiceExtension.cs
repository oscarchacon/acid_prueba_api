﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Token;
using Services.Utils.Authorization.Models;
using Microsoft.Extensions.Options;
using Swashbuckle.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.Swagger;
using System.Web.Http.Description;
using System.Reflection;
using DataAccess.Utils;
using Newtonsoft.Json;
using ISchemaFilter = Swashbuckle.Swagger.ISchemaFilter;
using System.IO;

namespace Acid_Prueba_Api.Extensions
{
    /// <summary>
    /// Clase Estatica para las Extensiones de Services.
    /// </summary>
    public static class ServiceExtension
    {
        /// <summary>
        /// Register the Swagger generator, defining one or more Swagger documents
        /// </summary>
        /// <param name="services">Services</param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Acid_Prueba_Api", Version = "v1" });
                // Set the comments path for the Swagger JSON and UI.

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, "WebApiEasySwagger.xml");
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.EnableAnnotations();
            });
        }

        /// <summary>
        /// Configura la autorización para el uso de token de Okta (desactivado por ahora)
        /// </summary>
        /// <param name="services">Services</param>
        /// <param name="Configuration">Configuration</param>
        public static void ConfigureAuthorization(this IServiceCollection services, IConfiguration Configuration)
        {
            var okta = Configuration.GetSection("Okta").Get<OktaModel>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.Authority = okta.Authority;
                        options.Audience = okta.Audience;
                        options.RequireHttpsMetadata = false;
                    });
        }

        /// <summary>
        /// Configura el SDK de Okta
        /// </summary>
        /// <param name="services">Services</param>
        /// <param name="Configuration">Configuration</param>
        public static void ConfigureOkta(this IServiceCollection services, IConfiguration Configuration)
        {
            var oktaConfig = Configuration.GetSection("OktaConfig");
            services.Configure<OktaConfigModel>(oktaConfig);

            services.AddSingleton(sp => sp.GetRequiredService<IOptions<OktaConfigModel>>().Value);

            services.AddSingleton<ITokenService, TokenService>();
        }

    }
}
